package cn.codeforfun;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class JavaTest {
    @Test
    void test() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        String result = encoder.encode("user");
        System.out.println(result);
    }
}
