package cn.codeforfun.endpoint;

import cn.codeforfun.utils.BaseTest;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.security.oauth2.common.util.OAuth2Utils.GRANT_TYPE;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ClientCredentialsTest extends BaseTest {

    @Test
    void test() throws Exception {
        mockMvc.perform(
                post("/oauth/token")
                        .with(httpBasic(clientId, clientSecret))
                        .param(GRANT_TYPE, "client_credentials")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.access_token", notNullValue()))
                .andExpect(jsonPath("$.token_type", notNullValue()))
                .andExpect(jsonPath("$.expires_in", notNullValue()))
                .andExpect(jsonPath("$.scope", notNullValue()))
                .andReturn();
    }
}
