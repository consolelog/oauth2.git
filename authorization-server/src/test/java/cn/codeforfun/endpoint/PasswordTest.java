package cn.codeforfun.endpoint;

import cn.codeforfun.utils.BaseTest;
import cn.codeforfun.utils.JsonUtils;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Map;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.security.oauth2.common.util.OAuth2Utils.GRANT_TYPE;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PasswordTest extends BaseTest {

    @Test
    void test() throws Exception {
        /*
        密码方式获取token
         */
        MvcResult mvcResult = mockMvc.perform(
                post("/oauth/token")
                        .with(httpBasic(clientId, clientSecret))
                        .param(GRANT_TYPE, "password")
                        .param("username", username)
                        .param("password", password)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.access_token", notNullValue()))
                .andExpect(jsonPath("$.token_type", notNullValue()))
                .andExpect(jsonPath("$.refresh_token", notNullValue()))
                .andExpect(jsonPath("$.expires_in", notNullValue()))
                .andExpect(jsonPath("$.scope", notNullValue()))
                .andReturn();
        String responseString = mvcResult.getResponse().getContentAsString();
        Map<String, Object> responseMap = JsonUtils.toMap(responseString);
        String accessToken = (String) responseMap.get("access_token");
        String refreshToken = (String) responseMap.get("refresh_token");

        /*
        检查token
         */
        mockMvc.perform(
                post("/oauth/check_token")
                        .with(httpBasic(clientId, clientSecret))
                        .param("token", accessToken)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$.scope").isNotEmpty())
                .andExpect(jsonPath("$.scope").isArray())
                .andExpect(jsonPath("$.exp").isNotEmpty())
                .andExpect(jsonPath("$.exp").isNumber())
                .andExpect(jsonPath("$.user_name").isNotEmpty())
                .andExpect(jsonPath("$.user_name").value(username))
                .andExpect(jsonPath("$.client_id").value(clientId))
                .andExpect(jsonPath("$.authorities").isNotEmpty())
                .andExpect(jsonPath("$.authorities").isArray())
        ;

         /*
        刷新token
         */
        mvcResult = mockMvc.perform(
                post("/oauth/token")
                        .with(httpBasic(clientId, clientSecret))
                        .param(GRANT_TYPE, "refresh_token")
                        .param("refresh_token", refreshToken)
        )
                .andExpect(status().isOk())
                .andReturn();
        responseString = mvcResult.getResponse().getContentAsString();
        responseMap = JsonUtils.toMap(responseString);
        String accessTokenNew = (String) responseMap.get("access_token");
        assertNotEquals(accessToken, accessTokenNew);
    }
}
