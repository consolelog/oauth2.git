package cn.codeforfun.endpoint;

import cn.codeforfun.utils.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.security.oauth2.common.util.OAuth2Utils.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AuthorizationCodeTest extends BaseTest {

    @Test
    void test() throws Exception {
        /*
        通过url获取code
         */
        MvcResult mvcResult = mockMvc.perform(
                get("/oauth/authorize")
                        .with(httpBasic(username, password))
                        .param(RESPONSE_TYPE, "code")
                        .param(REDIRECT_URI, redirectUri)
                        .param(CLIENT_ID, clientId)
        )
                .andExpect(status().isFound())
                .andExpect(header().string("Location", containsString("code=")))
                .andReturn();
        String location = mvcResult.getResponse().getHeader("Location");
        assertNotNull(location);
        Matcher matcher = Pattern.compile("^[\\S\\s]+code=(?<code>\\S+)[&\\S\\s]*$", Pattern.CASE_INSENSITIVE).matcher(location);
        assertTrue(matcher.find());

        String code = matcher.group("code");

        /*
        通过code获取token
         */
        mockMvc.perform(
                post("/oauth/token")
                        .with(httpBasic(clientId, clientSecret))
                        .param(GRANT_TYPE, "authorization_code")
                        .param(REDIRECT_URI, redirectUri)
                        .param("code", code)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.access_token", notNullValue()))
                .andExpect(jsonPath("$.token_type", notNullValue()))
                .andExpect(jsonPath("$.refresh_token", notNullValue()))
                .andExpect(jsonPath("$.expires_in", notNullValue()))
                .andExpect(jsonPath("$.scope", notNullValue()))
        ;
    }
}
