package cn.codeforfun.endpoint;

import cn.codeforfun.utils.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ImplicitTest extends BaseTest {
    @Test
    void test() throws Exception {
        /*
        implicit模式获取token
         */
        MvcResult result = mockMvc.perform(
                get("/oauth/authorize")
                        .with(httpBasic(username, password))
                        .param(OAuth2Utils.RESPONSE_TYPE, "token")
                        .param(OAuth2Utils.CLIENT_ID, clientId)
                        .param(OAuth2Utils.REDIRECT_URI, redirectUri)
        )
                .andExpect(status().isFound())
                .andReturn();
        String redirectedUrl = result.getResponse().getRedirectedUrl();
        assertNotNull(redirectedUrl);
        assertTrue(redirectedUrl.startsWith(redirectUri));
        assertTrue(redirectedUrl.contains("access_token="));
        assertTrue(redirectedUrl.contains("token_type="));
        assertTrue(redirectedUrl.contains("expires_in="));
    }
}
