package cn.codeforfun.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义注销成功后的跳转逻辑
 *
 * @author wangbin
 */
@Component
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        String key = "redirect";
        String redirect;
        if (!ObjectUtils.isEmpty(request.getHeader(key))) {
            redirect = request.getHeader(key);
        } else if (!ObjectUtils.isEmpty(request.getParameter(key))) {
            redirect = request.getParameter(key);
        } else {
            return;
        }
        response.sendRedirect(redirect);
    }
}
