package cn.codeforfun.user;


import cn.codeforfun.utils.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserTest extends BaseTest {
    /**
     * 以匿名用户权限访问user
     */
    @Test
    void publicMethod() throws Exception {
        mockMvc.perform(get("/user/public"))
                .andExpect(status().isOk())
        ;
    }

    /**
     * 以admin权限访问用户列表
     *
     * @see UserController#userList()
     */
    @Test
    @WithMockUser(authorities = {"admin"})
    void userList() throws Exception {
        mockMvc.perform(get("/user/list"))
                .andExpect(status().isOk())
        ;
    }

    /**
     * 以任意登陆用户权限访问用户列表
     *
     * @see UserController#userList()
     */
    @Test
    @WithMockUser
    void userList_failed() throws Exception {
        mockMvc.perform(get("/user/list"))
                .andExpect(status().isForbidden())
        ;
    }

    /**
     * 以匿名用户权限访问用户列表
     *
     * @see UserController#userList()
     */
    @Test
    void userList_failed_anonymous() throws Exception {
        mockMvc.perform(get("/user/list"))
                .andExpect(status().isUnauthorized())
        ;
    }
}
