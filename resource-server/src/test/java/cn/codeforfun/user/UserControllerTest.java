package cn.codeforfun.user;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UserControllerTest {
    @Resource
    MockMvc mockMvc;
    @MockBean
    UserService userService;

    /**
     * 以模拟用户的方式访问私有方法
     *
     * @see UserController#privateMethod()
     */
    @Test
    @WithMockUser
    void privateMethod() throws Exception {
        mockMvc.perform(get("/user/private"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("hello, world"))
        ;
    }

    /**
     * 以匿名用户的方式访问私有方法
     *
     * @see UserController#privateMethod()
     */
    @Test
    void privateMethod_failed() throws Exception {
        mockMvc.perform(get("/user/private"))
                .andExpect(status().isUnauthorized())
        ;
    }

    /**
     * 以模拟用户的方式访问公有方法
     * 由于WebMvcTest为了提升性能仅仅实例化了Web层，所以不会载入  {@link cn.codeforfun.config.ResourceServerConfiguration}，也就不知道公有方法。
     * 所以在需要载入 {@link cn.codeforfun.config.ResourceServerConfiguration} 的时候就不能使用WebMvcTest进行单元测试了
     *
     * @see UserController#publicMethod()
     */
    @Test
    @WithMockUser
    void publicMethod() throws Exception {
        mockMvc.perform(get("/user/public"))
                .andExpect(status().isOk())
        ;
    }

    /**
     * 以匿名用户的方式访问公有方法
     *
     * @see UserController#publicMethod()
     */
    @Test
    void publicMethod_failed() throws Exception {
        mockMvc.perform(get("/user/public"))
                .andExpect(status().isUnauthorized())
        ;
    }
}