package cn.codeforfun.user;

import cn.codeforfun.generator.model.User;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wangbin
 */
@RestController
public class UserController {
    @Resource
    UserService userService;

    /**
     * 私有方法
     */
    @GetMapping("/user/private")
    public String privateMethod() {
        return "hello, world";
    }

    /**
     * 公有方法
     */
    @GetMapping("/user/public")
    public String publicMethod() {
        return "success";
    }

    /**
     * 需要特殊权限的方法
     */
    @GetMapping("/user/list")
    @PreAuthorize("hasAnyAuthority('admin')")
    public List<User> userList() {
        return userService.userList();
    }
}
