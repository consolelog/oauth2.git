package cn.codeforfun.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangbin
 */
public class SecurityUtils {
    private static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static String getUsername() {
        return getAuthentication().getName();
    }

    public static List<GrantedAuthority> getAuthorities() {
        return new ArrayList<>(getAuthentication().getAuthorities());
    }
}
