create table t_client
(
    client_id                             varchar(36)  not null primary key,
    client_secret                         varchar(200) not null,
    client_access_token_validity_seconds  int          null comment 'access_token 过期时间，单位：秒',
    client_refresh_token_validity_seconds int          null comment 'refresh_token 过期时间，单位：秒',
    client_authorized_grant_types         varchar(200) null comment '授权类型',
    client_registered_redirect_uri        varchar(200) null comment '跳转 uri',
    client_scope                          varchar(200) null,
    client_resource_ids                   varchar(200) null,
    client_authorities                    varchar(200) null,
    client_additional_information         varchar(200) null,
    client_auto_approve_scope             varchar(200) null
)
    comment 'client表';

create table t_resource
(
    id          varchar(36)  not null primary key,
    name        varchar(200) not null comment '资源名',
    description varchar(200) not null comment '资源描述',
    constraint t_resource_name_unique_index unique (name)
)
    comment '资源表';

create table t_role
(
    id          varchar(36)  not null primary key,
    name        varchar(200) not null comment '角色名',
    description varchar(200) not null comment '角色描述',
    constraint t_role_name_unique_index unique (name)
)
    comment '角色表';

create table t_role_resource
(
    role_id     varchar(36) not null,
    resource_id varchar(36) not null,
    primary key (role_id, resource_id),
    constraint r_role_resource_resource_id_fk foreign key (resource_id) references t_resource (id),
    constraint r_role_resource_role_id_fk foreign key (role_id) references t_role (id)
)
    comment '角色资源中间表';

create table t_user
(
    id       varchar(36)      not null primary key,
    username varchar(200)     not null comment '用户名',
    password varchar(200)     not null comment '密码',
    enable   bit default b'1' not null comment '是否可用，1为可用，0为不可用',
    constraint t_user_name_unique_index unique (username)
)
    comment '用户表';

create table t_user_role
(
    user_id varchar(36) not null,
    role_id varchar(36) not null,
    primary key (user_id, role_id),
    constraint r_user_role_relation_role_id_fk foreign key (role_id) references t_role (id),
    constraint r_user_role_relation_user_id_fk foreign key (user_id) references t_user (id)
)
    comment '用户角色中间表';

