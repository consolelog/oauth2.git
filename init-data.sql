# client表
# client_id:client
# client_secret:secret
insert ignore into t_client (id, client_id, client_secret,
                             client_access_token_validity_seconds, client_refresh_token_validity_seconds,
                             client_authorized_grant_types,
                             client_registered_redirect_uri, client_scope, client_auto_approve_scope)
values ('client', 'client', '$2a$10$4exGEs2hdi0C5C8wiDzirOft5WLC7/WJFXVqz7cXhVw6/tnFOstZi',
        7200, 604800,
        'authorization_code,password,client_credentials,implicit,refresh_token',
        'http://localhost:4200/login', 'app', 'app');
#用户表
# username:admin
# password:admin
insert ignore into t_user (id, username, password)
values ('admin', 'admin', '$2a$10$P05jOa7SIFEPf58XrPk1euoFcAgzLavabdLUXd8uFIrHZ/pflZr02'),
       ('user', 'user', '$2a$10$UYfUeE54zgz3eb5sMDt2B.I7DZhXQxlboFHAkQjMGe3grcAmWXQCa');
# 角色表
insert ignore into t_role (id, name, description)
values ('user', 'user', '普通用户'),
       ('admin', 'admin', '系统管理员')
;
# 资源表
insert ignore into t_resource (id, name, description)
values ('user', 'user', '普通用户资源'),
       ('admin', 'admin', '管理员资源')
;
# 用户角色关系表
insert ignore into t_user_role (user_id, role_id)
values ('admin', 'admin'),
       ('admin', 'user'),
       ('user', 'user');
# 角色资源关系表
insert ignore into t_role_resource (role_id, resource_id)
values ('user', 'user'),
       ('admin', 'admin'),
       ('admin', 'user');