import {Component, OnInit} from '@angular/core';
import {CommonService} from './pages/service/common.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  auth: {} = {
    user_name: '',
    exp: null,
    active: null,
    authorities: []
  };

  ngOnInit(): void {
    this.auth = this.common.getAuth();
  }

  constructor(
    private common: CommonService,
    private router: Router,
  ) {
    common.authObservable.subscribe(res => {
      this.auth = res;
    });
  }

  codeLogin(): void {
    const loginHost = 'http://localhost:8080/oauth/authorize';
    const redirectUrl = 'http://localhost:4200/login';
    window.location.href = `${loginHost}?response_type=code&client_id=client&redirect_uri=${redirectUrl}&scope=app`;
  }

  logout(): void {
    this.common.logout();
    this.common.clear();
    this.router.navigate(['/public']);
  }
}
