import {Component, OnInit} from '@angular/core';
import {CommonService} from '../service/common.service';

@Component({
  selector: 'app-private',
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.scss']
})
export class PrivateComponent implements OnInit {
  msg;
  token = {};

  constructor(
    private common: CommonService,
  ) {
  }

  ngOnInit(): void {
    this.token = this.common.getToken();
    if (!this.token || !this.token[`access_token`]) {
      this.msg = '没有权限';
    } else {
      this.common.privateMethod(this.token[`access_token`]).subscribe(res => {
        this.msg = res;
      }, error => {
        this.msg = error.message;
      });
    }
  }

}
