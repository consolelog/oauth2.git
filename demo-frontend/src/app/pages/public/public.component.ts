import {Component, OnInit} from '@angular/core';
import {CommonService} from '../service/common.service';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.scss']
})
export class PublicComponent implements OnInit {
  msg;

  constructor(
    private common: CommonService,
  ) {
  }

  ngOnInit(): void {
    this.common.publicMethod().subscribe(res => {
      this.msg = res;
    });
  }

}
