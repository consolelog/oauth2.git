import {Component, OnInit} from '@angular/core';
import {CommonService} from '../service/common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user = {
    username: '',
    password: ''
  };

  auth: {} = {
    user_name: '',
    exp: null,
    active: null,
    authorities: []
  };

  token = {};

  constructor(
    private common: CommonService,
  ) {
    common.authObservable.subscribe(res => {
      this.auth = res;
    });
    common.tokenObservable.subscribe(res => {
      this.token = res;
    });
  }

  ngOnInit(): void {
    this.auth = this.common.getAuth();
    this.token = this.common.getToken();
  }

  submit(): void {
    this.common.login(this.user.username, this.user.password);
  }
}
