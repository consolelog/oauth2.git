import {Component, OnInit} from '@angular/core';
import {CommonService} from '../service/common.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  msg;
  token = {};
  list;

  constructor(
    private common: CommonService,
  ) {
  }

  ngOnInit(): void {
    this.token = this.common.getToken();
    if (!this.token || !this.token[`access_token`]) {
      this.msg = '没有权限';
    } else {
      this.common.adminMethod(this.token[`access_token`]).subscribe(res => {
        this.list = res;
      }, error => {
        this.msg = error.message;
      });
    }
  }

}
