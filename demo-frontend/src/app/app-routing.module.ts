import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PublicComponent} from './pages/public/public.component';
import {PrivateComponent} from './pages/private/private.component';
import {AdminComponent} from './pages/admin/admin.component';
import {LoginComponent} from './pages/login/login.component';
import {LoginGuard} from './pages/service/login.guard';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/public'},
  {path: 'public', component: PublicComponent},
  {path: 'private', component: PrivateComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'login', component: LoginComponent, canActivate: [LoginGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
