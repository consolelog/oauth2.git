package cn.codeforfun.user.mapper;

import cn.codeforfun.user.UserVO;
import org.apache.ibatis.annotations.Param;

public interface MoreUserMapper {
    UserVO selectByUsername(@Param("username") String username);
}
