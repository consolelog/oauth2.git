package cn.codeforfun.user;

import cn.codeforfun.generator.mapper.UserMapper;
import cn.codeforfun.generator.model.User;
import cn.codeforfun.user.mapper.MoreUserMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wangbin
 */
@Service
public class UserService implements UserDetailsService {
    @Resource
    MoreUserMapper moreUserMapper;
    @Resource
    UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserVO userVO = moreUserMapper.selectByUsername(username);
        if (userVO == null) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        return userVO;
    }

    public List<User> userList() {
        return userMapper.selectAll().stream().map(UserVO::from).collect(Collectors.toList());
    }
}
