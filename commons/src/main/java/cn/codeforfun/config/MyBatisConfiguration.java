package cn.codeforfun.config;

import org.springframework.context.annotation.Configuration;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author wangbin
 */
@Configuration
@MapperScan({"cn.codeforfun.**.mapper"})
public class MyBatisConfiguration {
}
