package cn.codeforfun.generator.model;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 表名：t_role_resource
 * 表注释：角色资源中间表
*/
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "t_role_resource")
public class RoleResource {
    @Id
    @Column(name = "role_id")
    private String roleId;

    @Id
    @Column(name = "resource_id")
    private String resourceId;
}