package cn.codeforfun.generator.model;

import java.util.Date;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 表名：t_client
 * 表注释：client表
*/
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "t_client")
public class Client {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private String id;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "client_secret")
    private String clientSecret;

    /**
     * access_token 过期时间，单位：秒
     */
    @Column(name = "client_access_token_validity_seconds")
    private Integer clientAccessTokenValiditySeconds;

    /**
     * refresh_token 过期时间，单位：秒
     */
    @Column(name = "client_refresh_token_validity_seconds")
    private Integer clientRefreshTokenValiditySeconds;

    /**
     * 授权类型
     */
    @Column(name = "client_authorized_grant_types")
    private String clientAuthorizedGrantTypes;

    /**
     * 跳转 uri
     */
    @Column(name = "client_registered_redirect_uri")
    private String clientRegisteredRedirectUri;

    @Column(name = "client_scope")
    private String clientScope;

    @Column(name = "client_resource_ids")
    private String clientResourceIds;

    @Column(name = "client_authorities")
    private String clientAuthorities;

    @Column(name = "client_additional_information")
    private String clientAdditionalInformation;

    @Column(name = "client_auto_approve_scope")
    private String clientAutoApproveScope;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "delete_flag")
    private Boolean deleteFlag;
}