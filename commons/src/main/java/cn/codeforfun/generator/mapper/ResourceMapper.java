package cn.codeforfun.generator.mapper;

import cn.codeforfun.generator.model.Resource;
import tk.mybatis.mapper.common.Mapper;

public interface ResourceMapper extends Mapper<Resource> {
}