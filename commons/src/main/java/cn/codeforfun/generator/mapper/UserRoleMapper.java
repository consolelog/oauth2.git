package cn.codeforfun.generator.mapper;

import cn.codeforfun.generator.model.UserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<UserRole> {
}