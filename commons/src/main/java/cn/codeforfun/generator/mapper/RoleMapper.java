package cn.codeforfun.generator.mapper;

import cn.codeforfun.generator.model.Role;
import tk.mybatis.mapper.common.Mapper;

public interface RoleMapper extends Mapper<Role> {
}