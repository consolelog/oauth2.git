package cn.codeforfun.generator.mapper;

import cn.codeforfun.generator.model.Client;
import tk.mybatis.mapper.common.Mapper;

public interface ClientMapper extends Mapper<Client> {
}