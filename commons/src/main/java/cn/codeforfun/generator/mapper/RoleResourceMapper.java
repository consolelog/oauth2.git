package cn.codeforfun.generator.mapper;

import cn.codeforfun.generator.model.RoleResource;
import tk.mybatis.mapper.common.Mapper;

public interface RoleResourceMapper extends Mapper<RoleResource> {
}