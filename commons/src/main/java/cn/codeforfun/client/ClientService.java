package cn.codeforfun.client;

import cn.codeforfun.generator.mapper.ClientMapper;
import cn.codeforfun.generator.model.Client;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;

/**
 * @author wangbin
 */
@Service
public class ClientService implements ClientDetailsService {
    @Resource
    ClientMapper clientMapper;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        Example example = Example.builder(Client.class).build();
        example.createCriteria().andEqualTo("clientId", clientId);
        Client client = clientMapper.selectOneByExample(example);
        return ClientVO.from(client);
    }
}
